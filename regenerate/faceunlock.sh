#!/bin/bash
echo "Regenerating FaceUnlock..."

ROOTDIR="$PWD"
CURRENT_DIR="$ROOTDIR/external/utils/regenerate"

cd packages/apps/Settings
git am $CURRENT_DIR/0001-face-settings-commits.patch

cd $ROOTDIR/frameworks/base
git am $CURRENT_DIR/0001-face-base-commits.patch

cd $ROOTDIR
echo "Done."
