#!/bin/bash
echo "Regenerating AOSP-Wfd Patches..."

ROOTDIR="$PWD"
CURRENT_DIR="$ROOTDIR/external/utils/regenerate"

cd $ROOTDIR/frameworks/av
git am "$CURRENT_DIR/0001-Revert-Removed-unused-class-and-its-test.patch"
git am "$CURRENT_DIR/0001-Revert-stagefright-remove-Miracast-sender-code.patch"
git am "$CURRENT_DIR/0001-libstagefright_wfd-libmediaplayer2-compilation-fixes.patch"
git am "$CURRENT_DIR/0001-stagefright-Fix-SurfaceMediaSource-getting-handle-fr.patch"
git am "$CURRENT_DIR/0001-stagefright-Fix-buffer-handle-retrieval-in-signalBuf.patch"
git am "$CURRENT_DIR/0001-libstagefright_wfd-video-encoder-does-not-actually-r.patch"
git am "$CURRENT_DIR/0001-Revert-Move-unused-classes-out-of-stagefright-founda.patch"
git am "$CURRENT_DIR/0001-Remove-libmediaextractor-dependency-from-libstagefri.patch"
git am "$CURRENT_DIR/0001-audioflinger-Fix-audio-for-WifiDisplay.patch"

cd $ROOTDIR
echo "Done."
